$(document).ready(function () {
    var autocompleteSelected = '';
    /*  function Ajax */
    function getData(url, params) {
        var apikey = "2olBwEh1W3QonupcbT3835mveGF8VHi7";
        var getParams = $.param(params);
        var requestUrl = 'https://api.sandbox.amadeus.com/v1.2/' + url + '?apikey=' + apikey + '&' + getParams;
        $.ajax({
            dataType: "json",
            url: requestUrl,
            type: 'GET',
            async: false,

        });
    }
    /*  function Ajax Autocomplete */


    function getDataAuto(url, params) {
        var apikey = "2olBwEh1W3QonupcbT3835mveGF8VHi7";
        var getParams = $.param(params);
        var requestUrl = 'https://api.sandbox.amadeus.com/v1.2/' + url + '?apikey=' + apikey + '&' + getParams;
        var ResponseData = '';
        $.ajax({
            dataType: "json",
            url: requestUrl,
            type: 'GET',
            async: false,
            success: function (data) {
                ResponseData = data;

            },

        });

        return ResponseData;
    }

    /*  function  Inspiration Search 
         Params: 
            origin :string (required)
    */
    function InspirationSearch(data) {
        console.log(data);
        //do some stuff
    }
    /*  function  Location Information 
         Params: 
            Code :string (required)
    */
    function LocationInformation(data, textStatus, jqXHR) {
        console.log('data', data);
        console.log('textStatus', textStatus);
        console.log('jqXHR', jqXHR);

        //do some stuff
    }
    /*  function  Airport Autocomplete
         Params: 
             term : String (required)
             country : String
             all_airports : Boolean
    */
    function AirportAutocomplete(data) {
        console.log('AirportAutocomplete:', data)
        console.log('autocompleteSelected:', autocompleteSelected)

        //do some stuff
    }
    /*  function  Extensive Search
         Params: 
             origin : String (required)
             destination : String (required)
             departure_date : date (yyyy-MM-dd)
             one-way : boolean 
             duration : Int
             direct : Boolean
             max_price : DAY / STAY /  DESTINATION /WEEK
    
    
    
    */
    function ExtensiveSearch(data) {
        console.log(data);
        //do some stuff
    }

    // getData('flights/inspiration-search',{'origin':'NYC'}).done(InspirationSearch);
    // getData('location/NCE/',{}).done(LocationInformation);
    // getData('airports/autocomplete',{'term':'NYC'}).done(AirportAutocomplete);
    // getData('flights/extensive-search',{'origin':'NCE','destination':'NYC','departure_date':'2018-05-01'}).done(ExtensiveSearch);

    $("#citystarting,#cityend").autocomplete({
        source: function (request, response) {
            autocompleteSelected = this.element[0].id;
            var dataresponse = getDataAuto('airports/autocomplete', { 'term': request.term });
            console.log('dataresponse',dataresponse);
            response(dataresponse);
        },
        minLength: 3,
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
          },
          select: function(event, ui) {
            $(this).val(ui.item.value + ' ' + ui.item.label);
    
            return false;
          }
        

        })
        .data("autocomplete")._renderItem = function(ul, item) {
          return $("<li>")
            .data("item.autocomplete", item)
            .append("<a>"+ item.label + "</a>")
            .appendTo(ul);
        };
});